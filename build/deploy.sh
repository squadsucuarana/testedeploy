#You are now in build folder and you should go to folder, which contains your code. In my example the code is in folder "src" outside of the "build" directory.
cd ..

#Here you make an archive
tar -zcvf testedeploy.tar.gz src/

#Here you put your archive to EC2 via SSH
scp testedeploy.tar.gz ubuntu@ip-172-31-15-169:/home/ubuntu

#Here you extract your archive on EC2 using SSH
ssh ubuntu@ip-172-31-15-169 tar -xvzf testedeploy.tar.gz